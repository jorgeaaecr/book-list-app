import { BookService } from './../../services/book.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CartComponent } from './cart.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  DebugElement,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { By } from '@angular/platform-browser';

const listBook: Book[] = [
  { name: '', author: '', isbn: '', price: 1000, amount: 2 },
  { name: '', author: '', isbn: '', price: 2000, amount: 2 },
  { name: '', author: '', isbn: '', price: 3000, amount: 2 },
];

describe('Cart component', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;
  let service: BookService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CartComponent],
      providers: [BookService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(BookService);
    // Al espiar el servicio y devolver el listbook
    // evitamos el uso del servicio real, esto por que
    // lo estamos utilizando en el OnInit
    spyOn(service, 'getBooksFromCart').and.callFake(() => listBook);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getTotalPrice returns an amount', () => {
    const totalPrice = component.getTotalPrice(listBook);
    expect(totalPrice).not.toBeNull();
    expect(totalPrice).toEqual(12000);
  });

  it('onInputNumberChange increments correctly', () => {
    const action = 'plus';
    const book = { name: '', author: '', isbn: '', price: 1000, amount: 2 };
    // const service = (component as any)._bookservice;
    // const service2 = component['_bookService'];
    // const service = fixture.debugElement.injector.get(BookService);
    const spyUAB = spyOn(service, 'updateAmountBook').and.callFake(() => {
      return null;
    });
    const spyGTP = spyOn(component, 'getTotalPrice').and.callFake(() => {
      return null;
    });

    expect(book.amount).toBe(2);
    component.onInputNumberChange(action, book);
    expect(book.amount).toBe(3);

    expect(spyUAB).toHaveBeenCalled();
    expect(spyGTP).toHaveBeenCalled();
  });

  it('onInputNumberChange decrements correctly', () => {
    const action = 'minus';
    const book = { name: '', author: '', isbn: '', price: 1000, amount: 2 };
    const spyUAB = spyOn(service, 'updateAmountBook').and.callFake(() => {
      return null;
    });
    const spyGTP = spyOn(component, 'getTotalPrice').and.callFake(() => {
      return null;
    });

    expect(book.amount).toBe(2);
    component.onInputNumberChange(action, book);
    expect(book.amount).toBe(1);

    expect(spyUAB).toHaveBeenCalled();
    expect(spyGTP).toHaveBeenCalled();
  });

  /* Forma correcta de probar un metodo privado */
  it('onClearBooks works correctly', () => {
    const spyCLCB = spyOn(
      component as any,
      '_clearListCartBook'
    ).and.callThrough();
    const spyRBFC = spyOn(service, 'removeBooksFromCart').and.callFake(() => {
      return null;
    });
    component.listCartBook = listBook;
    component.onClearBooks();
    expect(component.listCartBook.length).toBe(0);
    expect(spyCLCB).toHaveBeenCalled();
    expect(spyRBFC).toHaveBeenCalled();
  });

  /* Evitar probar metodos privados */
  it('_clearListCartBook works correctly', () => {
    const spyRBFC = spyOn(service, 'removeBooksFromCart').and.callFake(() => {
      return null;
    });
    component.listCartBook = listBook;
    component['_clearListCartBook']();
    expect(component.listCartBook.length).toBe(0);
    expect(spyRBFC).toHaveBeenCalled();
  });

  /* Prueba de integración */
  it('The title "The cart is empty" is not displayed when there is a list', () => {
    component.listCartBook = listBook;
    fixture.detectChanges();
    const debugElement: DebugElement = fixture.debugElement.query(
      By.css('#titleCartEmpty')
    );
    expect(debugElement).toBeFalsy();
  });

  it('The title "The cart is empty" is displayed correctly when the list is empty', () => {
    component.listCartBook = [];
    fixture.detectChanges();
    const debugElement: DebugElement = fixture.debugElement.query(
      By.css('#titleCartEmpty')
    );
    expect(debugElement).toBeTruthy();
    if (debugElement) {
      const element: HTMLElement = debugElement.nativeElement;
      expect(element.innerHTML).toContain('The cart is empty');
    }
  });
});
