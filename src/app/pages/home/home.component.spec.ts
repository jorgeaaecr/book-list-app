import { BookService } from './../../services/book.service';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Pipe,
  PipeTransform,
} from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Book } from 'src/app/models/book.model';
import { of } from 'rxjs';

const listBook: Book[] = [
  { name: '', author: '', isbn: '', price: 1000, amount: 2 },
  { name: '', author: '', isbn: '', price: 2000, amount: 2 },
  { name: '', author: '', isbn: '', price: 3000, amount: 2 },
];

const bookServiceMock = {
  getBooks: () => of(listBook),
};

@Pipe({ name: 'reduceText' })
class ReduceTextPipeMock implements PipeTransform {
  transform(): string {
    return '';
  }
}

describe('Home Component', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HomeComponent, ReduceTextPipeMock],
      providers: [
        //BookService
        { provide: BookService, useValue: bookServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getBook from the subscriptions', () => {
    const bookService = fixture.debugElement.injector.get(BookService);
    const listBook: Book[] = [];
    const spy = spyOn(bookService, 'getBooks').and.returnValue(of(listBook));
    component.getBooks();
    expect(spy).toHaveBeenCalled();
    expect(component.listBook).toBe(listBook);
  });
});
