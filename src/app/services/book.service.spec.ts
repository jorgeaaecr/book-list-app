import swal from 'sweetalert2';
import { Book } from './../models/book.model';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { BookService } from './book.service';
import { environment } from 'src/environments/environment.prod';

const listBook: Book[] = [
  { name: '', author: '', isbn: '', price: 1000, amount: 2 },
  { name: '', author: '', isbn: '', price: 2000, amount: 2 },
  { name: '', author: '', isbn: '', price: 3000, amount: 2 },
];

const bookA: Book = {
  id: 'A',
  name: 'Book A',
  author: 'Autor A',
  isbn: 'A',
  price: 1000,
  amount: 2,
};
const bookB: Book = {
  id: 'B',
  name: 'Book B',
  author: 'Author B',
  isbn: 'B',
  price: 2000,
  amount: 2,
};

describe('bookService', () => {
  let service: BookService;
  let httpMock: HttpTestingController;
  let storage = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    service = TestBed.inject(BookService);
    httpMock = TestBed.inject(HttpTestingController);
    storage = {};
    spyOn(localStorage, 'getItem').and.callFake((key: string) => {
      return storage[key] || null;
    });
    spyOn(localStorage, 'setItem').and.callFake(
      (key: string, value: string) => {
        return (storage[key] = value);
      }
    );
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('getBook, return a list of book and does a get method', () => {
    service.getBooks().subscribe((resp: Book[]) => {
      expect(resp).toEqual(listBook);
    });
    const req = httpMock.expectOne(environment.API_REST_URL + '/book');
    expect(req.request.method).toBe('GET');
    req.flush(listBook);
  });

  it('getBooksFromCart, return empty array when localStorage is empty', () => {
    const listBook = service.getBooksFromCart();
    expect(listBook.length).toBe(0);
  });

  it('addBookToCart, add a book when the list book does not exist and exists in the localStorage', async () => {
    const toast = {
      fire: () => null,
    } as any;
    const spyA = spyOn(swal, 'mixin').and.callFake(() => {
      return toast;
    });
    // Test list book does not exists
    let listBook = service.getBooksFromCart();
    expect(listBook.length).toBe(0);
    service.addBookToCart(bookA);
    listBook = service.getBooksFromCart();
    expect(listBook.length).toBe(1);
    expect(spyA).toBeTruthy();

    // Test book exits
    service.addBookToCart(bookA);
    listBook = service.getBooksFromCart();
    expect(listBook[0].amount).toBe(2);

    // Test book does not exists
    await service.addBookToCart(bookB);

    listBook = service.getBooksFromCart();
    console.log(listBook);
    expect(listBook.length).toBe(2);
  });

  it('removeBooksFromCart removes the list from the localStorage', () => {
    service.addBookToCart(bookA);
    let listBook = service.getBooksFromCart();
    expect(listBook.length).toBe(1);
    service.removeBooksFromCart();
    listBook = service.getBooksFromCart();
    expect(listBook.length).toBe(0);
  });
});
